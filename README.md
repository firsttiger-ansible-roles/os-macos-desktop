# os-macos-desktop

## Summary

The os-macos-desktop installs all the critical software required for proper functioning of a Macbook. This role will install apps using 3 methods.

* DMG files that have to either be downloaded first or downloaded from a local source

* The Apple App store. 

You can install an app via homebrew called mas (Mac App Store)

The id of the app can be found by using:

```bash
mas search APP_NAME 
```

You install an app via command line

```bash
mas install [app_id]
```

* Homebrew for technology related applications

## Pre-requisites

* Xcode is installed
* Homebrew is installed

```bash
brew install ansible
```

## Application groups

### Core Apps

* Chrome
* Bitwarden
* Twingate
* Edge
* Divvy
* Raycast
* MacUpdater

### Development Apps and Utilities

* Visual Studio Code
* Oh-My-Zsh
* rbenv
* Docker Desktop
* Valentia Studio

# Additional settings

## Developer settings

Typically a variable with the list of developers is stored in inventories/group_vars/all/global.yml
The format of the hash looks like this

```yml
  development_users:
  - {
      username: "kelvin",
      ssh_public_key_file: "files/key_files/kelvin_kang.pub",
      password: "[hashed password]"
    }
```

## Variables

| Variable name            | Description                                                                  | Setup       |
|--------------------------|------------------------------------------------------------------------------|-------------|
| local_hostname           | Internal hostname of the server                                              | core        |
| home_directory           | User home directory                                                          | core        |
| download_directory       | Download directory                                                           | core        |
| project_directory        | Project directory                                                            | development |

## Settings

| Variable name            | Description                            | Values                                          | File   |
|--------------------------|----------------------------------------|-------------------------------------------------|--------|
| external_networking      | Sets up for external access            | true; false                                     | host   |
